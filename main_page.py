import string

from bottle import route, run, template, request
from bottle import redirect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import time
from bs4 import BeautifulSoup
import requests
import math
import collections

Base = declarative_base()
engine = create_engine("sqlite:///news.db")
session = sessionmaker(bind=engine)


def extract_next_page(url):
    r = requests.get(url)
    page = BeautifulSoup(r.text, 'html5lib')
    news = page.table.findAll('table')
    news = news[1]
    link = news.find('a', {'class': 'morelink'})
    link = str(link)
    link = link[32:]
    index = link.index(" ")
    link = link[:index - 1]
    print(link)
    return link


def extract_news(url):
    r = requests.get(url)
    page = BeautifulSoup(r.text, 'html5lib')
    title_list = []
    titles = []
    urls = []
    points = []
    authors = []
    comments = []
    news = page.table.findAll('table')
    news = news[1]
    comment = news.findAll('a', rel=False, id=False)
    for i in comment:
        i = str(i)
        if 'item?' in i and 'ago' not in i:
            index = i.index('>')
            i = i[index + 1:]
            index = i.index('<')
            i = i[:index]
            if len(i) > 20:
                continue
            else:
                if i == 'discuss':
                    comments.append(0)
                else:
                    try:
                        index = i.index('c')
                        i = i[:index - 1]
                        comments.append(i)
                    except ValueError:
                        comments.append(0)

    author = news.findAll('a', {'class': 'hnuser'})
    for i in author:
        i = str(i)
        index = i.index('>')
        i = i[index + 1:]
        index = i.index('<')
        i = i[:index]
        authors.append(i)
    score = news.findAll('span', {'class': 'score'})
    for i in score:
        i = str(i)
        index = i.index('>')
        i = i[index + 1:]
        index = i.index('<')
        i = i[:index]
        i, _ = i.split(' ')
        points.append(i)
    td_news = news.findAll('td', {'class': 'title'})
    for i in range(1, 60):
        try:
            title = td_news[i].find('a', {'class': 'storylink'})
            title_list.append(title)
        except IndexError:
            continue
    try:
        for i in range(40):
            title_list.remove(None)
    except ValueError:
        pass
    for i in title_list:
        k = str(i)
        index_k = k.index('/')
        k = k[index_k + 2:]
        try:
            index_k = k.index(" ")
            k = k[:index_k - 1]
            urls.append(k)
        except ValueError:
            urls.append('None')
        i = str(i)
        index = i.index('>')
        string = i[index + 1:]
        index = string.index('<')
        string = string[:index]
        titles.append(string)
    return comments, authors, points, titles, urls


def get_news(url, count=1):
    link = ''
    url_ = url + link
    news = []
    for i in range(count):
        comments, authors, points, titles, urls = extract_news(url_)
        for i in range(30):
            dict = {
                'author': authors[i],
                'comments': comments[i],
                'points': points[i],
                'title': titles[i],
                'url': urls[i]
            }
            news.append(dict)
        link = extract_next_page(url_)
        url_ = url + link
        print(url_)
        time.sleep(30)
    return news


class News(Base):
    __tablename__ = "news"
    id = Column(Integer, primary_key=True)
    title = Column(String)
    author = Column(String)
    url = Column(String)
    comments = Column(Integer)
    points = Column(Integer)
    label = Column(String)


Base.metadata.create_all(bind=engine)


@route('/')
def news_list():
    s = session()
    rows = s.query(News).filter(News.label == None).all()
    return template('news_template', rows=rows)


@route('/add_label/')
def add_label():
    string = str(request.get)
    index = string.index('=')
    string = string[index + 1:]
    index_2 = string.index('&')
    label = string[:index_2]
    id = string[index_2 + 4:-2]
    id = int(id)
    news = s.query(News).filter(News.id == id)
    for k in news:
        k.label = label
    s.commit()
    redirect('/')


@route('/update_news/')
def update_news():
    unique = True
    new_news = get_news("https://news.ycombinator.com/newest")
    s = session()
    news = s.query(News).all()
    for i in new_news:
        for k in news:
            if i['title'] == k.title and i['author'] == k.author:
                unique = False
        if unique:
            new = News(title=i['title'], author=i['author'], url=i['url'], comments=i['comments'], points=i['points'])
            s.add(new)
        unique = True
    s.commit()
    redirect('/')


def clean(s):
    word_list = []
    word_list.extend(s.lower().rstrip(string.punctuation).split())
    return word_list


class NaiveBayesClassifier:

    def __init__(self, alpha=1):
        self.alpha = alpha
        self.labels = []
        self.table = []
        self.p_labels = []

    def fit(self, x, y):
        """ Fit Naive Bayes classifier according to X, y. """
        self.labels = [i for i in set(y)]
        self.labels.sort()
        classes = len(self.labels)
        labels_count = [0] * classes
        for i in range(len(y)):
            y[i] = self.labels.index(y[i]) + 1
            labels_count[y[i] - 1] += 1

        self.table = [[] for _ in range(classes * 2 + 1)]
        self.p_labels = [math.log(number / sum(labels_count)) for number in labels_count]

        for i in range(len(x)):
            c = str(x[i])
            words = clean(c)
            for word in words:
                if word in self.table[0]:
                    self.table[y[i]][self.table[0].index(word)] += 1
                else:
                    self.table[0].append(word)
                    self.table[y[i]].append(1)
                    index = y[i]
                    for j in range(classes - 1):
                        index = (index % classes) + 1
                        self.table[index].append(0)
                    for column in range(classes + 1, classes * 2 + 1):
                        self.table[column].append(0)

        sums = [sum(self.table[i + 1]) for i in range(classes)]
        dim = len(self.table[0])

        for line in range(dim):
            for column in range(classes + 1, classes * 2 + 1):
                self.table[column][line] = (self.table[column - classes][line] + self.alpha) / \
                                           (sums[column - classes - 1] + self.alpha * dim)


    def predict(self, x):
        """ Perform classification on an array of test vectors X. """
        labels = []
        classes = len(self.labels)
        for string in x:
            string_labels = [i for i in self.p_labels]
            words = clean(string)
            for word in words:
                if word in self.table[0]:
                    for i in range(classes):
                        string_labels[i] += math.log(self.table[i + classes + 1][self.table[0].index(word)])
            for i in range(classes):
                if string_labels[i] == max(string_labels):
                    labels.append(self.labels[i])
                    break
        print(collections.Counter(labels))
        return labels

    def score(self, x_test, y_test):
        """ Returns the mean accuracy on the given test data and labels. """
        prediction = self.predict(x_test)
        count = 0
        for i in range(len(prediction)):
            if prediction[i] == y_test[i]:
                count += 1
        score = count / len(y_test)
        return score


@route('/recommendations')
def recommendations():
    recently_marked_news = s.query(News).filter((News.label != None)).all()
    x_extra_train = [row.title for row in recently_marked_news]
    y_extra_train = [row.label for row in recently_marked_news]
    model.fit(x_extra_train, y_extra_train)

    blank_rows = s.query(News).filter(News.label == None).all()
    x = [row.title for row in blank_rows]
    labels = model.predict(x)
    classified_news = [blank_rows[i] for i in range(len(blank_rows)) if labels[i] == 'good']
    return template('news_recommendations', rows=classified_news)


if __name__ == "__main__":
    s = session()
    model = NaiveBayesClassifier()
    marked_news = s.query(News).filter(News.label != None).all()
    x_train = [row.title for row in marked_news]
    y_train = [row.label for row in marked_news]
    model.fit(x_train, y_train)
    run(host="localhost", port=666)
